'use strict';

// Класс Vector, позволит контролировать расположение объектов в двухмерном пространстве и управлять их размером и перемещением.
class Vector {
	constructor(x = 0, y = 0) {
		this.x = x;
		this.y = y;
	}
	plus(vector) {
		if(!(vector instanceof Vector)) {
			throw new Error('Можно прибавлять к вектору только вектор типа Vector');
		}
		return new Vector(this.x + vector.x, this.y + vector.y);
	}
	times(multipler) {
		return new Vector(this.x * multipler, this.y * multipler);
	}
}

// Класс Actor, позволит контролировать все движущиеся объекты на игровом поле и контролировать их пересечение.
class Actor {
	constructor(pos = new Vector(), size = new Vector(1, 1), speed = new Vector()) {		
		if(!(pos instanceof Vector)) {
			throw new Error('Параметр pos должен иметь тип Vector');
		}
		if(!(size instanceof Vector)) {
			throw new Error('Параметр size должен иметь тип Vector');
		}
		if(!(speed instanceof Vector)) {
			throw new Error('Параметр speed должен иметь тип Vector');
		}
		
		this.pos = pos;
		this.size = size;
		this.speed = speed;
	}
	act() {
	}
	get left() {
		return this.pos.x;
	}
	get top() {
		return this.pos.y;
	}
	get right() {
		return this.pos.x + this.size.x;
	}
	get bottom() {
		return this.pos.y + this.size.y;
	}
	get type() {
		return 'actor';
	}
	// Метод проверяет, пересекается ли текущий объект с переданным объектом.
	isIntersect(actor) {
		if(!(actor instanceof Actor)) {
			throw new Error('Объект проверки на пересечение должен иметь тип Actor');
		}
		
		if(this === actor) {
			return false;
		}
		
		return this.left < actor.right && this.right > actor.left && this.top < actor.bottom && this.bottom > actor.top;
	}
}

/* В задании ничего не сказано о ориентации координатных осей.
 Для оси X примем привычное направление слева-направо, а для оси Y направление сверху-вниз.
 Как для направлений векторов, так и для ячеек игрового поля.
*/

// Объекты класса Level реализуют схему игрового поля конкретного уровня, 
// контролируют все движущиеся объекты на нём и реализуют логику игры
class Level {
	constructor(grid, actors = []) {
		this.grid = grid;
		this.actors = actors;
		
		if(actors instanceof Array) {
		  this.player = actors.find(element => element.type === 'player');
		} else {
		  this.player = undefined;
		}
		if(grid instanceof Array) {
			this.height = grid.length;
			this.width = grid.reduce((previous, current) => Math.max(previous, current.length), 0);
		} else {
			this.height = 0;
			this.width = 0;
		}
		this.status = null;
		this.finishDelay = 1;
	}
	// Определяет, завершен ли уровень.
	isFinished() {
		return this.status !== null && this.finishDelay < 0;
	}
	// Определяет, расположен ли какой-то другой движущийся объект в переданной позиции.
	actorAt(actor) {
		if(!(actor instanceof Actor)) {
			throw new Error('Объект проверки на пересечение должен иметь тип Actor');
		}
		return this.actors.find(element => actor.isIntersect(element));
	}
	// Определяет, нет ли препятствия в указанном месте.  
	// Также контролирует выход объекта за границы игрового поля.
	obstacleAt(pos, size) {
		if(!(pos instanceof Vector)) {
			throw new Error('Параметр pos должен иметь тип Vector');
		}
		if(!(size instanceof Vector)) {
			throw new Error('Параметр size должен иметь тип Vector');
		}
		
		if(pos.y + size.y > this.height) {
			return 'lava';
		}
		if(pos.x < 0 || pos.x + size.x > this.width || pos.y < 0) {
			return 'wall';
		}
		
		/* С учетом того, что передаем размер объекта, возможно перекрывание им области состоящей из более чем одной ячейки. Т.е. вообще говоря  мы можем пересечь сразу несколько объектов. Возвращать будем первый, что возможно не совсем верно.
		*/
		for(let y = Math.floor(pos.y); y < pos.y + size.y; y++) {
			for(let x = Math.floor(pos.x); x < pos.x + size.x; x++) {
				let obstacle = this.grid[y][x];
				if(obstacle !== undefined) {
					return obstacle;
				}
			}
		}
		
		return undefined;
	}
	// Метод удаляет переданный объект с игрового поля.
	removeActor(actor) {
		let indexActor = this.actors.indexOf(actor);
		if(indexActor != -1) {
			this.actors.splice(indexActor, 1);
		}
	}
	// Определяет, остались ли еще объекты переданного типа на игровом поле.
	noMoreActors(type) {
		return !this.actors.some(actor => actor.type === type);
	}
	// Меняет состояние игрового поля при касании игроком каких-либо объектов или препятствий.
	playerTouched(type, actor) {
		if(this.status !== null) {
			return;
		} else if(type === 'lava' || type === 'fireball') {
			this.status = 'lost';
		} else if(type === 'coin' && actor.type === type) {
			this.removeActor(actor);
			if(this.noMoreActors(type)) {
				this.status = 'won';
			}
		}
	}
}

// Объект класса LevelParser позволяет создать игровое поле Level из массива строк.
class LevelParser {
	constructor(dictActors = []) {
		this.dictActors = dictActors;
	}
	// Возвращает конструктор объекта по его символу, используя словарь.
	actorFromSymbol(symbol) {
		return symbol === undefined ? undefined : this.dictActors[symbol];
	}
	// Возвращает строку, соответствующую символу препятствия.
	obstacleFromSymbol(symbol) {
	  let type;
		switch (symbol) {
			case 'x':
				type = 'wall';
				break;
			case '!':
				type = 'lava';
				break;
		}
		return type;
	}
	// Принимает массив строк и преобразует его в массив массивов, в ячейках которого хранится либо строка, соответствующая препятствию, либо undefined
	createGrid(plan) {
		return plan.map(line => {
		  let cells = [];
		  for(let sym of line) {
		    cells.push(this.obstacleFromSymbol(sym));
		  }
		  return cells;
		})
	}
	// Принимает массив строк и преобразует его в массив движущихся объектов, используя для их создания конструкторы из словаря.
	createActors(plan) {
		let actors = [];
		plan.forEach((line, y) => {
		  for(let x = 0; x < line.length; x++) {
				let Constr = this.dictActors[line[x]];
				let obj = Object(Constr);
				if(obj === Actor || obj.prototype instanceof Actor) {
					actors.push(new Constr(new Vector(x, y)));
				}
			}
		})
		return actors;
	}
	// Принимает массив строк, создает и возвращает игровое поле, заполненное препятствиями и движущимися объектами, полученными на основе символов и словаря.
	parse(plan) {
		return new Level(this.createGrid(plan), this.createActors(plan));
	}
}

// Класс Fireball станет прототипом для движущихся опасностей на игровом поле.
class Fireball extends Actor {
  constructor(pos = new Vector(), speed = new Vector()) {
    super(pos, new Vector(1, 1), speed);
  }
	get type() {
		return 'fireball';
	}
	// Создает и возвращает вектор Vector следующей позиции шаровой молнии.
  getNextPosition(time = 1) {
    return this.pos.plus(this.speed.times(time));
  }
  // Обрабатывает столкновение молнии с препятствием.
  handleObstacle() {
    this.speed = this.speed.times(-1);
  }
  // Обновляет состояние движущегося объекта.
  act(time, level) {
    let nextPos = this.getNextPosition(time);
    let obstacle = level.obstacleAt(nextPos, this.size);
    // !!! В задании сказано, что метод obstacleAt должен возвращать тип препятствия строкой, либо undefined.
    // !!! В тесте предполагается, что метод obstacleAt возвращает булевый тип, из-за чего диагностировалась ошибка. Мне кажется тест не совсем корректен.
    //if(obstacle !== undefined) {
    if(obstacle) {
      this.handleObstacle();
    } else {
      this.pos = nextPos;
    }
  }
}

// Горизонтальная шаровая молния
class HorizontalFireball extends Fireball {
  constructor(pos = new Vector()) {
    super(pos, new Vector(2, 0));
  }
}

// Вертикальная шаровая молния
class VerticalFireball extends Fireball {
  constructor(pos = new Vector()) {
    super(pos, new Vector(0, 2));
  }
}

// Огненный дождь
class FireRain extends Fireball {
  constructor(pos = new Vector()) {
    super(pos, new Vector(0, 3));
    this.basePos = pos;
  }
  handleObstacle() {
    this.pos = this.basePos;
  }
}

// Класс Coin реализует поведение монетки на игровом поле.
class Coin extends Actor {
  constructor(pos = new Vector()) {
    super(pos.plus(new Vector(0.2, 0.1)), new Vector(0.6, 0.6));
    this.basePos = this.pos;
    this.springSpeed = 8;
    this.springDist = 0.07;
    this.spring = Math.random() * Math.PI * 2;
  }
  get type() {
    return 'coin';
  }
  // Обновляет фазу подпрыгивания. 
  updateSpring(time = 1) {
    this.spring += this.springSpeed * time;
  }
  // Создает и возвращает вектор подпрыгивания.
  getSpringVector() {
    return new Vector(0, Math.sin(this.spring) * this.springDist);
  }
  // Обновляет текущую фазу, создает и возвращает вектор новой позиции монетки.
  getNextPosition(time = 1) {
    this.updateSpring(time);
    return this.basePos.plus(this.getSpringVector());
  }
  // Получает новую позицию объекта и задает её как текущую.
  act(time) {
    this.pos = this.getNextPosition(time);
  }
}

class Player extends Actor {
  constructor(pos = new Vector()) {
    super(pos.plus(new Vector(0, -0.5)), new Vector(0.8, 1.5), new Vector(0, 0));
  }
  get type() {
    return 'player';
  }
}

const actorDict = {
  '@': Player,
  'v': FireRain,
  'o': Coin,
  '|': VerticalFireball,
  '=': HorizontalFireball
}

/* !!! В логике игры выявил небольшой баг позволяющий существенно обегчить прохождение ряда уровней, - игрок может "карабкаться" по стенам (включая "виртуальные", которые ограничивают уровень слева и справа). Для этого нужно жать кнопку соответствующую текущему расположениею стены по отношению к игроку (стрелки лево/право), одновременно нажимая стрелку вверх.
*/

const parser = new LevelParser(actorDict);
loadLevels()
	.then(stringLevels => {
		let schemas = JSON.parse(stringLevels);
		runGame(schemas, parser, DOMDisplay)
			.then(() => alert('Вы выиграли приз!'))
		}
	)